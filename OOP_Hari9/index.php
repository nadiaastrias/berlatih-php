<?php
    require("animal.php");
    require("Ape.php");
    require("Frog.php");

    $sheep = new Animal("shaun");

    echo "<h3>Soal No 1</h3>";
    echo "Nama : ".$sheep->name . "<br>"; // "shaun"
    echo "legs : ".$sheep->legs. "<br>"; // 4
    echo "Cold Blooded : " .$sheep->cold_blooded."<br><br>"; // "no"

    $sungokong = new Ape("kera sakti");
    echo "<h3>Soal No 2</h3>";
    echo "Nama : ".$sungokong->name . "<br>"; 
    echo "legs : ".$sungokong->legs. "<br>"; 
    echo "Cold Blooded : " .$sungokong->cold_blooded."<br>"; 
    echo "yell : ";
    $sungokong->yell()."<br>";// "Auooo

    $kodok = new Frog("buduk");
    echo "<h3>Soal No 3</h3>";
    echo "Nama : ".$kodok->name . "<br>";
    echo "legs : ".$kodok->legs. "<br>"; 
    echo "Cold Blooded : " .$kodok->cold_blooded."<br>"; 
    echo "jump : ";
    $kodok->jump()."<br>"; // "hop hop"
?>