<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <p>First name:</p>
        <input type="text" id="fname" name="fname" ><br>

        <p>Last name:<p>
        <input type="text" id="lname" name="lname" ><br>

        <p>Gender:</p>
        <input type="radio" id="male" name="gender" value="Male">
        <label>Male</label><br>
        <input type="radio" id="female" name ="gender" value="Female">
        <label>Female</label><br>
        <input type="radio" id="other" name ="gender" value="Other">
        <label>Other</label><br>
        
        <p>Nationallity:</p>
        <select name="nationallity">
          <option value="indonesian">indonesian</option>
          <option value="american">american</option>
          <option value="canadian">canadian</option>
          <option value="indian">indian</option>
        </select>

        <p>Language Spoken:</p>
        <input type="checkbox" name ="bahasa1" value="Bahasa Indonesia">
        <label for="bahasa1">Bahasa Indonesia</label><br>
        <input type="checkbox" name ="bahasa2" value="English">
        <label for="bahasa2">English</label><br>
        <input type="checkbox" name ="bahasa3" value="Other">
        <label for="bahasa3">Other</label><br>

        <p>Bio:</p>
        <textarea name="message" rows="10" cols="30"></textarea>
        <br>

        <input type="submit" value="Sign Up">
      </form> 
      
</body>
</html>