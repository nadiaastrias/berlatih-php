@extends('admin.master')
@section('title')
    Edit Cast {{$cast->id}}
@endsection

@section('content')

<div>

        <form action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" value="{{$cast->nama}}" name="nama" id="nama" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">umur</label>
                <input type="number" class="form-control" value="{{$cast->umur}}" name="umur" id="umur" placeholder="1">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                  <label>Bio</label>
                  <textarea class="form-control" name ="bio" id= "bio" rows="3" placeholder="Masukkan Bio">{{$cast->bio}}</textarea>
                  @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
@endsection